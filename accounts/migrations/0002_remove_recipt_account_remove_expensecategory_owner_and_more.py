# Generated by Django 5.0.1 on 2024-01-31 16:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("accounts", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="recipt",
            name="account",
        ),
        migrations.RemoveField(
            model_name="expensecategory",
            name="owner",
        ),
        migrations.RemoveField(
            model_name="recipt",
            name="category",
        ),
        migrations.RemoveField(
            model_name="recipt",
            name="purchaser",
        ),
        migrations.DeleteModel(
            name="Account",
        ),
        migrations.DeleteModel(
            name="ExpenseCategory",
        ),
        migrations.DeleteModel(
            name="Recipt",
        ),
    ]
